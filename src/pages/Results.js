import React from 'react';
import { useHistory } from 'react-router-dom';
import TableRow from '../components/TableRow';

const Results = ({records}) => {
    const history = useHistory();

    const handleGame = () => {
        history.push('/');
    }
    return(
        <div>
            <button className="btn" onClick={handleGame}>Игра</button>
            <div className="table header">
                <div className="table_cell-id">№</div>
                <div className="table_cell-name">ИМЯ</div>
                <div className="table_cell-time">ВРЕМЯ</div>
                <div className="table_cell-date">ДАТА</div>
            </div>
            <div className="table_wrapper">
                {records.map(((record, ind) => {
                    return <TableRow key={record.date.toString()} record={record} id={ind+1} />})
                )}
            </div>
        </div>
    );
};
export default Results;
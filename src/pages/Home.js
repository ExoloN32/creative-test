import React, {useState} from 'react';
import { useHistory } from 'react-router-dom';
import './Home.css';
import Card from '../components/Card';

const pressed = [];
let timer = null;

const Home = ({start, stop, end}) => {
    const history = useHistory();
    const [cards, setCards] = useState([]);

    const handleResults = () => {
        stop();
        history.push('/results');
    }

    const handleCard = (index) => {
        if(pressed.length === 2 || cards[index].open === 'card open no') {
            return;
        }
        if(pressed.length === 1 && pressed[0] === index) {
            return;
        }
        pressed.push(index)
        setCards((prev) => {
            const newCards = [...prev];
            newCards[index].open = 'card open';
            if(pressed.length === 1) {
                timer = setTimeout(() => {
                    if(pressed.length === 1) {
                        setCards((prev => {
                            const newCards = [...prev];
                            newCards[pressed[0]].open = 'card';
                            return newCards;
                        }));
                        pressed.length = 0;
                    }
                }, 5000)
            }
            return newCards;
        });
        if(pressed.length === 2) {
            clearTimeout(timer)
            setTimeout(() => {
                if(cards[pressed[0]].type === cards[pressed[1]].type) {
                    setCards((prev => {
                        const newCards = [...prev];
                        newCards[pressed[0]].open = 'card open no';
                        newCards[pressed[1]].open = 'card open no';
                        for(let i=0; i<newCards.length; i++) {
                            if(newCards[i].open !== 'card open no') {
                                return newCards;
                            }
                        }
                        setTimeout(end, 1000);
                        return newCards;
                    }));
                } else {
                    setCards((prev => {
                        const newCards = [...prev];
                        newCards[pressed[0]].open = 'card';
                        newCards[pressed[1]].open = 'card';
                        return newCards;
                    }));
                }
                pressed.length = 0;
            }, 1000)
        }
    };

    const handleStart = () => {
        start();
        const newCards = [];
        for(let i=1; i<37; i++) {
            newCards.push({id: i, open: 'card', type: Math.ceil(i/2)-1});
        };
        for (let i = newCards.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [newCards[i], newCards[j]] = [newCards[j], newCards[i]];
        };
        setCards(newCards);
    }

    return(
        <div className="home-wrapper">
            <div className="home_nav">
                <button className="btn" onClick={handleResults}>Результаты</button>
            </div>
            <div className="message"></div>
            <button className="btn" onClick={handleStart}>Старт</button>
            <div className="grid-container">
                {cards.map((card, index) => {
                    return(<Card key={card.id} data={card} click={handleCard} ind={index} />);
                })}
            </div>
        </div>
    );
};

export default Home;
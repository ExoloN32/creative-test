import React from 'react';
import './Card.css';
import ac_unit from '../assets/ac_unit.svg';
import access_alarm from '../assets/access_alarm.svg';
import accessibility_new from '../assets/accessibility_new.svg';
import account_balance from '../assets/account_balance.svg';
import add_box from '../assets/add_box.svg';
import agriculture from '../assets/agriculture.svg';
import air from '../assets/air.svg';
import airplanemode_active from '../assets/airplanemode_active.svg';
import airport_shuttle from '../assets/airport_shuttle.svg';
import align_vertical_center from '../assets/align_vertical_center.svg';
import all_inclusive from '../assets/all_inclusive.svg';
import anchor from '../assets/anchor.svg';
import animation from '../assets/animation.svg';
import apps from '../assets/apps.svg';
import architecture from '../assets/architecture.svg';
import attach_file from '../assets/attach_file.svg';
import auto_stories from '../assets/auto_stories.svg';
import back_hand from '../assets/back_hand.svg';

const icons = [
    ac_unit,
    access_alarm,
    accessibility_new,
    account_balance,
    add_box,
    agriculture,
    air,
    airplanemode_active,
    airport_shuttle,
    align_vertical_center,
    all_inclusive,
    anchor,
    animation,
    apps,
    architecture,
    attach_file,
    auto_stories,
    back_hand
];

const Card = ({data, click, ind}) => {

    return(
        <div className={data.open} onClick={() => click(ind)}>
            <div className="front"> 
                {/* {data.type} */}{/* Можно раскоментировать для тестирования */}
            </div>
            <div className="back">
                <img src={icons[data.type]} alt='' />
            </div>
        </div>
    );
}

export default Card;

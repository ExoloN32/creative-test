import React from 'react';
import { ParseTime } from '../services/ParseTime';

const Timer = ({time}) => {
    const parseTime = ParseTime(time);
        
    return(
        <div className="timer">
            {parseTime.h}:{parseTime.m}:{parseTime.s} 
        </div>
    );
};

export default Timer;
import React from 'react';
import { ParseTime } from '../services/ParseTime'

const TableRow = ({record, id}) => {
    const parseTime = ParseTime(record.time);

    return(
        <div className="table">
            <div className="table_cell-id">{id}</div>
            <div className="table_cell-name">{record.name}</div>
            <div className="table_cell-time">{parseTime.h}:{parseTime.m}:{parseTime.s}</div>
            <div className="table_cell-date">{new Date(record.date).toLocaleString()}</div>
        </div>
    );
};

export default TableRow;
import React, { useState } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import Results from './pages/Results';
import Timer from './components/Timer';

let beginTime = null;
let interval = null;

function App() {
  const [time, setTime] = useState(0);
  const [modal, setModal] = useState(false);
  const [name, setName] = useState('');
  const [records, setRecords] = useState([]);

  const startTime = () => {
    beginTime = Date.now();
    if(interval) {
      clearInterval(interval);
    };
    setTime(0);
    interval = setInterval(() => {
      setTime(Date.now() - beginTime);
    }, 1000);
  };

  const stopTime = () => {
    if(interval) {
      clearInterval(interval);
      interval = null;
    };
  };

  const endGame = () => {
    stopTime();
    setModal(true);
  };

  const handleChange = (event) => {
    setName(event.target.value);
  };

  const handleSubmit = () => {
    setModal(false);
    const record = {name: name, time: time, date: Date.now()};
    setRecords((prev) => {
      return [...prev, record].sort((a, b) => {
        if(a.time > b.time) {
          return 1;
        }
        return -1;
      });
    });
  };

  return (
    <div className="App">
      <div><Timer time={time} /></div>
      <BrowserRouter>
        <main>
          <Switch>
            <Route 
              exact
              path="/" 
              render={props => <Home start={startTime} stop={stopTime} end={endGame} {...props} />} 
            />
            <Route
              path="/results"
              render={props => <Results records={records} {...props} />}
            />
          </Switch>
        </main>
      </BrowserRouter>
      {modal && <div className="modal_wrapper">
        <div className="modal_card">Введите ваше имя.
          <input type="text" value={name} onChange={handleChange} />
          <div><button className="btn" onClick={handleSubmit}>Ввод</button></div>
        </div>
      </div>}
    </div>
  );
};

export default App;

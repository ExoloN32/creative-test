export const ParseTime = (time) => {
    let tm = new Date(time);
    let parseTime = {h: '00', m: '00', s: '00'};
    if(time !== 0) {
        const hour = tm.getUTCHours();
        const min = tm.getMinutes();
        const sec = tm.getSeconds();
        parseTime = {h: hour.toString(), m: min.toString(), s: sec.toString()}
        if(parseTime.h.length === 1) {
            parseTime.h = '0' + parseTime.h;
        };
        if(parseTime.m.length === 1) {
            parseTime.m = '0' + parseTime.m;
        };
        if(parseTime.s.length === 1) {
            parseTime.s = '0' + parseTime.s;
        };
    };
    return parseTime;
};